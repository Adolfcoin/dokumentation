Welcome to the Adolfcoin wiki!

## Using Adolfcoin
[How to use the Adolfcoin daemon (adolfnode)](https://github.com/Adolfcoin/Wiki/wiki/Using-Adolfnode)  
[How to use the Adolfcoin wallet](https://github.com/Adolfcoin/Wiki/wiki/Using-the-Adolfcoin-Wallet)

## The Führerprocess  

[The Führerprocess](https://github.com/Adolfcoin/Wiki/wiki/The-F%C3%BChrerprocess)  
This is the decentralized process used for developing Adolfcoin.  
[The Annotated Führerprocess](https://github.com/Adolfcoin/Wiki/wiki/Annotated-F%C3%BChrerprocess)  
An explanation of each line of the Führerprocess.  
[Simplicity Oriented Design](https://github.com/Adolfcoin/Wiki/wiki/Simplicity-Oriented-Design)  
Foundational principles for the Führerprocess.  
[Adolfcoin Style Guide](https://github.com/Adolfcoin/Wiki/wiki/Style-Guide)  
Please try to follow this style guide when submitting patches.  
[Adolfcoin Forking Protocol](https://github.com/Adolfcoin/Wiki/wiki/Adolfcoin-Forking-Protocol)  
How to make breaking changes to Adolfcoin's consensus protocol.  

## API References
Wallet API reference (json-rpc)  
Adolfnode API reference (json-rpc)  
  