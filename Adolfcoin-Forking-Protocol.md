-   Name: RFC2
-   Status: draft

The Adolfcoin Forking Protocol provides the process to follow if you wish to fork Adolfcoin.

License
-------
Copyright (c) AUTHORS listed at the end of this file.

This Specification is free software; you can redistribute it and/or modify it under the terms of the [GPL3](https://www.gnu.org/licenses/gpl-3.0.txt).

This Specification is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.

Abstract
--------

The Adolfcoin Forking Protocol is under active development. With more forks, we will have more data to refine this process.

Language
--------

The key words "MUST", "MUST NOT", "REQUIRED", "SHALL", "SHALL NOT", "SHOULD", "SHOULD NOT", "RECOMMENDED", "MAY", and "OPTIONAL" in this document are to be interpreted as described in [RFC 2119](http://tools.ietf.org/html/rfc2119).

1\. Goals
---------

While the Führerprocess governs individual Adolfcoin forks, it does not allow for breaking changes to consensus protocol without "overwhelming consensus" on the necessity of these changes. This forking protocol will allow forks (breaking changes to the consensus protocol) to occur without upfront consensus. 

Requirements of this protocol:

1. A valid fork MUST be located within the the Adolfcoin organization on Github to create a cohesive location where users can find all forks, view the source, contribute, and fork further.
2. Any contributor or maintainer must be able to fork Adolfcoin without discrimination. 
3. A fork shall only be valid if it is governed internally by the Fuhrerprocess
4. The Fuhrerprocess shall be used to update this protocol
5. All valid forks shall be accepted as currency on Adolf's Anonymous Marketplace
6. All forks of the Adolfcoin Daemon (Adolfnode) shall be named adolfcoin-xxx where xxx is a single English or German word describing the key difference between this fork and the parent.


AUTHORS
-------

The Führer   
Please add your name to the end of this list if you contribute to this protocol.