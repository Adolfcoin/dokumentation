In Linux, start the wallet by going to the binary directory and typing `./wallet`  
Windows users simply double click on the wallet application. 
 
Do this while adolfnode is running in a different window.  

The wallet will prompt you for a name:  
```Wallet file name:```  
Enter the name you want to use for your wallet. Use a single word (no spaces). This will create wallet files in the current directory.  

You will then see:  
```
The wallet doesn't exist, generating new one
password: 
```   
Enter a password, this will encrypt your wallet file.  
At the moment, the wallet does not ask you to confirm your password (so type carefully), but this should be added soonish.  

```
List of available languages for your wallet's seed:
0 : English
1 : Spanish
2 : Portuguese
3 : Japanese
Enter the number corresponding to the language of your choice: 
```
Pick a language. German and Kekistani are coming soon.  

You'll then see a list of 25 words. Write these down, they can be used to recover your wallet if you ever lose it.   

You should also see:
```[wallet xxxxx]:``` xxxxx will be replaced by first characters/numbers of your wallet address.  

Type `refresh` and it will sync with adolfnode.

Here are some other useful commands:   

```
  address              Show your public address (what you give to people to send you adolfcoin)
  balance              Show current wallet balance
  bc_height            Show blockchain height
  help                 Show this help
  incoming_transfers   Show transfers received by your wallet
  payments             payments <payment_id> Show a payment you've made
  refresh              Resynchronize transactions and balance
  seed                 Show your 25 word seed code
  set_log              verbosity (0-4)
  spendkey             Get your spendkey
  viewkey              Get viewkey
  start_mining         start_mining <number_of_threads> - Start mining in adolfnode
  stop_mining          Stop mining in daemon
  sweep_dust           Send all dust outputs to the same address with mixin 0
```

How to transfer coins to someone else:  
```
transfer <mixin count> <address to send to> <amount to send> <payment ID (optional)>
```

Mixin count: how many other transactions do you want this transfer to be indistinguishable from? A safe number to use is 3. Depending on the number of outputs that you have, you may need to lower this number or split your transfer up into smaller amounts. More explanation about this will come later.  

Payment ID: you probably won't need this until Adolf's Anonymous Marketplace is up and running. It's a way for other people to programmatically get your transfer (e.g. an online store can have a single receiving address but give every customer a different payment ID to use).