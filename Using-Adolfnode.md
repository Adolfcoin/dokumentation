To start Adolfnode in Linux simply type ./adolfnode in the directory where the binary is located. If you are using Windows you can simply double click on the adolfnode program.  

This is usually all you need to do to get up and running. 

Here are the command line options:  
```
Usage: ./adolfnode <commands>

Options:
  --help                                Produce help message
  --version                             Output version information
  --os-version                          OS for which this executable was 
                                        compiled
  --data-dir arg                        Specify data directory
  --testnet-data-dir arg                Same as above but for testnet
                                        
  --config-file arg                     Specify configuration file
  --test-drop-download                  For connection tests: discard all
                                        blocks instead checking/saving them 
  --test-drop-download-height arg (=0)  Same as above, but kick in after the 
                                        specified block number
                                        
  --test-dbg-lock-sleep arg (=0)        Sleep time in ms, defaults to 0 (off), 
                                        used to debug before/after locking 
                                        mutex. Values 100 to 1000 are good for 
                                        tests.

  --detach                              Run as daemon (in background)

Settings:
  --log-file arg                        Specify a log file
  --log-level arg                       Verbosity (0-4)
  --testnet                             Run on testnet. The wallet must be 
                                        launched with --testnet flag.
  --enforce-dns-checkpointing           Not fully implemented yet, don't use it.
  --extra-messages-file arg             Specify file for extra messages to 
                                        include into coinbase transactions
  --start-mining <WALLET ADDRESS>       Start mining
  --mining-threads arg                  Specify mining threads count
  --p2p-bind-ip arg                     Interface for p2p network protocol
  --p2p-bind-port arg                   Port for p2p network protocol
  --testnet-p2p-bind-port arg           Port for testnet p2p network protocol
  --allow-local-ip                      Allow local ip add to peer list (debugging)
  --add-peer arg                        Manually add peer
  --add-priority-node arg               Specify list of peers to connect to and
                                        attempt to keep the connection open
  --add-exclusive-node arg              Specify list of peers to connect to 
                                        only. If this option is given the 
                                        options add-priority-node and seed-node
                                        are ignored
  --seed-node arg                       Connect to this node and get their peerlist
  --hide-my-port                        Do not announce your node to the network
  --no-igd                              Disable UPnP port mapping
  --out-peers arg                       set max limit of out peers
  --tos-flag arg                        set TOS flag
  --rpc-bind-ip arg                     IP for RPC server
  --rpc-bind-port arg                   Port for RPC server
  --testnet-rpc-bind-port arg           Port for testnet RPC server
```

The following commands can be used from within Adolfnode when it's running:  

```
  diff               Show difficulty and current block being worked on
  exit               Save the blocktrain and exit
  fast_exit          GTFO, don't save nuff'n
  help               Show this help
  hide_hr            Stop logging hash rate
  limit              limit <kB/s> - Set download and upload limit
  limit_down         limit <kB/s> - Set download limit
  limit_up           limit <kB/s> - Set upload limit
  out_peers          Set max number of peers
  print_block        Print block, print_block <block_hash> OR <block_height>
  print_cn           Print live connections
  print_height       Print blocktrain height
  print_pl           Print peer list
  print_pool         Print transaction pool (long format)
  print_pool_sh      Print transaction pool (short format)
  print_status       Prints daemon status
  print_tx           Print transaction, print_tx <transaction_hash>
  save               Save blocktrain (happens periodically anyway)
  set_log            set_log <level> - verbosity (0-4)
  show_hr            Start logging network hash rate
  start_mining       start_mining <WALLET ADDRESS> 
  stop_daemon        Stop the daemon
  stop_mining        Stop mining
```